
Watchdog More - Provides a helper to empty the watchdog database table.

Requirements
--------------------------------------------------------------------------------
- This module is written for Drupal 6.0+.

- The database logging (core) module.

Installation
--------------------------------------------------------------------------------
Copy the Watchdog More module folder to your module directory and then enable
on the admin modules page.

USAGE
--------------------------------------------------------------------------------
Go to Administer -> Reports -> Recent log entries. The fieldset "Empty log"
provides a button to empty the database log table (table watchdog).
Note: Emptying occurs immediately without confirmation.

Maintainer
--------------------------------------------------------------------------------
Quiptime Group
Siegfried Neumann
www.quiptime.com
quiptime [ at ] gmail [ dot ] com